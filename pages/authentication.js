const {By, until} = require('selenium-webdriver');

//const BasePage = require('./../pages/base');
const CreateAccPage = require('./../pages/createaccount');

const url = 'http://automationpractice.com/index.php?controller=authentication&back=identity';
const pageHeaderText = 'Authentication';

const elements = {

        pageHeader: By.css('h1.page-heading'),

        // Login elements
        input_login_email: By.css('#email'),
        input_login_password: By.css('#passwd'),
        button_login: By.css('#SubmitLogin'),

        // Create account elements
        input_signup_email: By.css('#email_create'),
        button_signup: By.css('#SubmitCreate'),

        // Alerts
        alert: By.css('.alert'),

};

module.exports = class AuthPage {

    constructor(driver) {
        this.driver = driver;
    };

    /** Common Block */  // todo: move code to BasePage class

    static get pageHeaderText() {
        return pageHeaderText;
    };

    static get url() {
        return url;
    };

    async openPage() {
        await this.driver.get(url);
    };

    async readPageHeader() {
        return this.driver.findElement(elements.pageHeader).getText();
    };


    // async readErrorMessage() {
    //     await this.driver.wait(until.elementIsVisible(elements.alert));
    //     return this.driver.getText(elements.alert);
    // };

    /** Page specific methods */

    async createAccount(email) {

        await this.driver.findElement(elements.input_signup_email).sendKeys(email);
        await this.driver.findElement(elements.button_signup).click();

        await this.driver.wait(until.urlIs(CreateAccPage.url));

    };

    async login(email, password) {

        await this.driver.findElement(elements.input_login_email).sendKeys(email);
        await this.driver.findElement(elements.input_login_password).sendKeys(password);
        await this.driver.findElement(elements.button_login).click();

        // Todo: change to class prop when My Account page object created
        await this.driver.wait(until.urlIs('http://automationpractice.com/index.php?controller=my-account'));
    };

};