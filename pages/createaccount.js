const {By, until} = require('selenium-webdriver');

//const BasePage = require('./../pages/base');

const url = 'http://automationpractice.com/index.php?controller=authentication&back=identity#account-creation';
const pageHeaderText = 'Create an account';

const elements = {

    pageHeader: By.css('h1.page-heading'),

    // Personal info form elements
    radio_Mr: By.css('#id_gender1'),
    radio_Mrs: By.css('#id_gender2'),
    input_firstName: By.css('#customer_firstname'), // required field
    input_lastName: By.css('#customer_lastname'),   // required field
    input_email: By.css('#email'),                  // required field   // prefilled from prev page
    input_password: By.css('#passwd'),              // required field
    // todo: DOB                                    // field unmarked * though required

    checkbox_newsletter: By.css('#uniform-newsletter'),
    checkbox_offers: By.css('#uniform-optin'),

    // Address form elements
    input_address_firstName: By.css('#firstname'),  // required field   // prefilled from personal info
    input_address_lastName: By.css('#lastname'),    // required field   // prefilled from personal info
    input_company: By.css('#company'),
    input_addresLine1: By.css('#address1'),         // required field
    input_adressLine2: By.css('#address2'),
    input_city: By.css('#city'),                    // required field

    dropdown_state: By.css('#id_state'),        // required field // note! disappears of country != US
    option_state: By.css('#id_state > option:nth-child(2)'), // first option from the list

    input_zip: By.css('#postcode'),             // required field // note! disappears of country != US
    dropdown_country: By.css('#id_country'),    // required field
    // todo: dropdown list elements

    input_additionalInfo: By.css('#other'),
    input_phoneHome: By.css('#phone'),
    input_phoneMobile: By.css('#phone_mobile'), // required field
    input_alias: By.css('#alias'),                  // prefilled as 'My address'

    button_submit: By.css('#submitAccount'),


    // Alerts
    alert: By.css('.alert'),

};

module.exports = class AuthPage {

    constructor(driver) {
        this.driver = driver;
    };

    /** Common Block */  // todo: move code to BasePage class

    static get pageHeaderText() {
        return pageHeaderText;
    };

    static get url() {
        return url;
    };

    async openPage() {
        await this.driver.get(url);
    };

    async readPageHeader() {
        return this.driver.findElement(elements.pageHeader).getText();
    };

    // async readErrorMessage() {
    //     await this.driver.wait(until.elementIsVisible(elements.alert));
    //     return this.driver.getText(elements.alert);
    // };

    /** Page specific methods */

    async fillRegForm(user) {

        /** Personal Info */

        if (user.gender === 'm') {
            await this.driver.findElement(elements.radio_Mr).click();
        } else if (user.gender === 'f') {
            await this.driver.findElement(elements.radio_Mrs).click();
        }
        await this.driver.findElement(elements.input_firstName).sendKeys(user.firstName);
        await this.driver.findElement(elements.input_lastName).sendKeys(user.lastName);
        await this.driver.findElement(elements.input_email).clear();                        // clear prefilled data
        await this.driver.findElement(elements.input_email).sendKeys(user.email);
        await this.driver.findElement(elements.input_password).sendKeys(user.password);

        // todo: DOB

        if (user.newsletter) {
            await this.driver.findElement(elements.checkbox_newsletter).click();
        } // todo: add uncheck and move code to BasePage class
        if (user.offers != undefined) {
            await this.driver.findElement(elements.checkbox_offers).click();
        } // todo: add uncheck and move code to BasePage class

        /** Address */

        await this.driver.findElement(elements.input_address_firstName).clear();            // clear prefilled data
        await this.driver.findElement(elements.input_address_firstName).sendKeys(user.firstName);
        await this.driver.findElement(elements.input_address_lastName).clear();             // clear prefilled data
        await this.driver.findElement(elements.input_address_lastName).sendKeys(user.lastName);

        if (user.company != undefined) {
            await this.driver.findElement(elements.input_company).sendKeys(user.company);
        }

        await this.driver.findElement(elements.input_addresLine1).sendKeys(user.addressLine1);

        if (user.addressLine2 != undefined) {
            await this.driver.findElement(elements.input_adressLine2).sendKeys(user.addressLine2);
        }

        await this.driver.findElement(elements.input_city).sendKeys(user.city);

        // todo: state
        await this.driver.findElement(elements.dropdown_state).click();
        //await this.driver.wait(until.elementIsEnabled(elements.option_state));
        await this.driver.findElement(elements.option_state).click();

        await this.driver.findElement(elements.input_zip).sendKeys(user.zip);

        // todo: country // prefilled as US

        if (user.other != undefined) {
            await this.driver.findElement(elements.input_additionalInfo).sendKeys(user.other);
        }
        if (user.phoneHome != undefined) {
            await this.driver.findElement(elements.input_phoneHome).sendKeys(user.phoneHome);
        }
        if (user.phoneMobile != undefined) {
            await this.driver.findElement(elements.input_phoneMobile).sendKeys(user.phoneMobile);
        }


        if (user.alias != undefined) {
            await this.driver.findElement(elements.input_alias).clear();
            await this.driver.findElement(elements.input_alias).sendKeys(user.alias);
        }

        await this.driver.findElement(elements.button_submit).click();

        // Todo: change to class prop when the following page object created
        await this.driver.wait(until.urlIs('http://automationpractice.com/index.php?controller=my-account'));

    };


};