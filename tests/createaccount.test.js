const {Builder} = require('selenium-webdriver');

const expect = require('chai').expect;

const AuthPage = require('./../pages/authentication');
const CreateAccPage = require('./../pages/createaccount');

describe('Create Account Test Suite', function()  {

    // set timeouts for Mocha tests
    this.slow(10000);
    this.timeout(50000);

    let driver;
    let page;


    beforeEach( async function() {

        driver = new Builder().forBrowser('chrome').build();

        // set implicit timeout for webdriver to allow more time to locate an element (0 by default)
        await driver.manage().setTimeouts({implicit: 5000});

        page = new AuthPage(driver);

        await page.openPage();

        // validate the page header matches Auth page
        const header = await page.readPageHeader();
        expect(header.toLowerCase()).to.equal(AuthPage.pageHeaderText.toLowerCase());

        // todo: add catching waiting for the page never loaded

    });

    it('should allow registration for a new user with valid email', async function() {

        const id = Math.random().toString(36).substring(2, 15); // random id
        // define test data   todo: move code to separate class and generate user automatically
        const user = {
            email: `my.precious.test.user+${id}@gmail.com`,
            password: '123456',
            firstName: 'Jane',
            lastName: 'Doe',
            dob: '',
            gender: 'f',
            company: 'ABC Ltd',
            addressLine1: '101 Street',
            zip: '12345',
            city: 'Big city',
            state: '',
            country: '',
            phoneMobile: '1234567890',

            newsletter: true,
            offers: false,

        };

        // fill the form and submit
        await page.createAccount(user.email);

        // validate the page header matches CreateAcc page
        const header = await page.readPageHeader();
        expect(header.toLowerCase()).to.equal(CreateAccPage.pageHeaderText.toLowerCase());

        page = new CreateAccPage(driver);

        await page.fillRegForm(user);

        // todo: validate the page header matches My Account page
        // url 'http://automationpractice.com/index.php?controller=my-account'
        // page header 'My account'

        const header2 = await page.readPageHeader();
        expect(header2.toLowerCase()).to.equal('My account'.toLowerCase());
        //expect(header.toLowerCase()).to.equal(MyAcc.pageHeaderText.toLowerCase());

    });

    afterEach( async function() {
        await driver.quit();
    })


});
